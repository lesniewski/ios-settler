import UIKit

class GroupViewController: UIViewController {
    
    // MARK: Properties
    private var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var membersLabel: UILabel!
    
    private var memberNameInputs = [UITextField]()
    
    var group: Group?
    
    var name: String {
        return nameTextField.text ?? ""
    }
    var membersNames: [String] {
        return memberNameInputs.map({ ($0.text ?? "").trim() }).filter({ $0 != "" })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addGroupNameInput()
        
        if let group = group {
            // Editing existing group
            nameTextField.text = group.name
            navigationItem.title = group.name
            
            listMembers(group.members)
        }
        else {
            // Hide delete button, when creating new group
            deleteButton.enabled = false
            deleteButton.tintColor = UIColor.clearColor()
            
            addMemberInput()
        }
    }
    
    private func listMembers(members: [Member]) {
        var viewAbove = membersLabel! as UIView
        
        for member in members {
            let nameLabel = UILabel()
            nameLabel.translatesAutoresizingMaskIntoConstraints = false
            
            nameLabel.text = member.name
            nameLabel.font = UIFont.systemFontOfSize(20)
            
            self.view.addSubview(nameLabel)
            
            Constraints.pinToSideMargins(nameLabel, margins: self.view.layoutMarginsGuide)
            Constraints.pinTopToBottom(nameLabel, viewAbove: viewAbove)
            
            viewAbove = nameLabel
        }
    }
    
    private func addMemberInput() {
        let memberInput = UITextField()
        memberInput.translatesAutoresizingMaskIntoConstraints = false
        
        memberInput.placeholder = "Member name"
        memberInput.returnKeyType = .Next
        memberInput.enablesReturnKeyAutomatically = true
        memberInput.font = UIFont.systemFontOfSize(14)
        memberInput.autocapitalizationType = .Words
        memberInput.autocorrectionType = .No
        memberInput.spellCheckingType = .No
        memberInput.borderStyle = .RoundedRect
        
        memberInput.addTarget(self, action: "memberInputEditingBegin:", forControlEvents: UIControlEvents.EditingDidBegin)
        
        self.view.addSubview(memberInput)
        
        Constraints.pinToSideMargins(memberInput, margins: self.view.layoutMarginsGuide)
        let viewAbove = memberNameInputs.last ?? membersLabel!
        Constraints.pinTopToBottom(memberInput, viewAbove: viewAbove)
        
        memberNameInputs += [memberInput]
    }
    
    func memberInputEditingBegin(sender: UITextField) {
        if (sender == memberNameInputs.last) {
            addMemberInput()
        }
    }
    
    private func addGroupNameInput() {
        let nameInput = UITextField()
        nameInput.translatesAutoresizingMaskIntoConstraints = false
        
        nameInput.placeholder = "Group name"
        nameInput.returnKeyType = .Next
        nameInput.enablesReturnKeyAutomatically = true
        nameInput.font = UIFont.systemFontOfSize(14)
        nameInput.autocapitalizationType = .Sentences
        nameInput.autocorrectionType = .No
        nameInput.spellCheckingType = .No
        nameInput.borderStyle = .RoundedRect
        
        self.view.addSubview(nameInput)
        
        Constraints.pinToSideMargins(nameInput, margins: self.view.layoutMarginsGuide)
        Constraints.pinTopToBottom(nameInput, viewAbove: nameLabel)
        Constraints.pinBottomToTop(nameInput, viewBelow: membersLabel)
        
        nameTextField = nameInput
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func saveChanges(sender: UIBarButtonItem) {
        let errors = getValidationErrors()
        guard errors.count == 0 else {
            showErrors(errors)
            return
        }
        
        let data = DataService.sharedInstance

        if let group = group {
            // Edit existing group
            group.name = name
        }
        else {
            // Create a new group
            group = data.createGroup(name)
            membersNames.map({ data.createMember(group!, name: $0) })
        }

        try! data.saveChanges()
        
        self.performSegueWithIdentifier("SaveChanges", sender: sender)
    }


    private func deleteGroup() {
        DataService.sharedInstance.deleteObject(group!)
        try! DataService.sharedInstance.saveChanges()
    }

    
    // MARK: Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if sender === deleteButton {
            deleteGroup()
        }
    }
    
    private func getValidationErrors() -> [String] {
        var errors = [String]()
        
        if name == "" {
            errors += ["What is the group name?"]
        }
        
        if group == nil && membersNames.count < 2 {
            errors += ["You need at least 2 members to form a group."]
        }
        
        return errors
    }
    
    func showErrors(errors: [String]) {
        let alertController = UIAlertController(title: "Missing", message: errors.reduce("", combine: { "\($0!)\n\($1)" }), preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Back", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    @IBAction func confirmDelete(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Are you sure?", message: "Do you really want to delete this group?", preferredStyle: .Alert)

        let cancelAction = UIAlertAction(title: "Nope", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)

        let destroyAction = UIAlertAction(title: "Delete", style: .Destructive) { (action) in
            self.performSegueWithIdentifier("DeleteGroup", sender: sender)
        }
        alertController.addAction(destroyAction)

        self.presentViewController(alertController, animated: true, completion: nil)
    }

    @IBAction func cancel(sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMode = presentingViewController is UINavigationController
        
        if isPresentingInAddMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
}

