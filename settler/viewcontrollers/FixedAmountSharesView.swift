import Foundation
import UIKit

class FixedAmountSharesView : UIView {
    
    var sharesViews = [FixedShareView]()
    var shares = [UnmanagedMemberShare]()
    var sharesBySharesViews = [FixedShareView: UnmanagedMemberShare]()
    var paymentTotal: Money = Money() {
        didSet {
            recalculateShares()
        }
    }
    
    func initialize(shares: [UnmanagedMemberShare]) {
        createUiElements(shares)
        layoutUiElements()
        recalculateShares()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func recalculateShares() {
        let unsplitAmountPerUnsetMember = getUnsplitAmountPerUnsetMember()
        
        for (shareView, share) in sharesBySharesViews {
            shareView.amount = share.hasValue ? share.value : unsplitAmountPerUnsetMember
            shareView.amountFixed = share.hasValue
            shareView.enabled = true
        }
        
        let unsetMembersCount = shares.filter({ !$0.hasValue }).count
        if unsetMembersCount == 1 {
            let lastUnsetShareView = sharesBySharesViews.filter({ !$0.1.hasValue }).map({ $0.0 })[0]
            lastUnsetShareView.enabled = false
        }
    }
    
    func shareChanged(sender: FixedShareView) {
        let share = sharesBySharesViews[sender]!
        if !share.hasValue || share.value != sender.amount {
            // User provided value
            sharesBySharesViews[sender]!.value = sender.amount
            recalculateShares()
        }
    }
    
    func fixDeleted(sender: FixedShareView) {
        if sharesBySharesViews[sender]!.hasValue {
            sharesBySharesViews[sender]!.clearValue()
            recalculateShares()
        }
    }
    
    private func getUnsplitAmountPerUnsetMember() -> Money {
        let splitAmount = shares.filter({ $0.hasValue }).map({ $0.value }).reduce(Money(), combine: { $0 + $1 })
        let unsplitAmount = paymentTotal - splitAmount
        let unsetMembersCount = shares.filter({ !$0.hasValue }).count
        let unsplitAmountPerUnsetMember = unsplitAmount / unsetMembersCount
        return unsplitAmountPerUnsetMember
    }
    
    private func createUiElements(shares: [UnmanagedMemberShare]) {
        self.backgroundColor = UIColor.whiteColor()
        
        self.shares += shares
        for share in shares {
            let shareView = FixedShareView()
            shareView.member = share.member
            shareView.amount = share.hasValue ? share.value : Money()
            shareView.amountFixed = share.hasValue
            shareView.enabled = true
            shareView.addTarget(self, action: "shareChanged:", forControlEvent: .AmountChanged)
            shareView.addTarget(self, action: "fixDeleted:", forControlEvent: .FixDeleted)
            
            sharesViews += [shareView]
            sharesBySharesViews[shareView] = share
            self.addSubview(shareView)
        }
    }
    
    private func layoutUiElements() {
        guard sharesViews.count >= 2 else {
            print("Fail: SharesView must have at least 2 shares.")
            return
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        pinToSideMargins(sharesViews[0])
        // Pin to top of the control
        sharesViews[0].topAnchor.constraintEqualToAnchor(self.topAnchor).active = true
        
        for var i = 1; i < sharesViews.count - 1; i++ {
            let shareView = sharesViews[i]
            pinToSideMargins(shareView)
            pinTopToBottomOf(sharesViews[i - 1], target: shareView)
        }
        
        pinToSideMargins(sharesViews[sharesViews.count - 1])
        // Pin to bottom of the control
        sharesViews[sharesViews.count - 1].bottomAnchor.constraintEqualToAnchor(self.bottomAnchor).active = true
    }
    
    private func pinToSideMargins(pinnedView: UIView) {
        let margins = self
        let leftMarginConstraint = pinnedView.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor)
        leftMarginConstraint.active = true
        let rightMarginConstraint = pinnedView.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor)
        rightMarginConstraint.active = true
    }
    
    private func pinTopToBottomOf(viewAbove: UIView, target: UIView) {
        let constraint = target.topAnchor.constraintEqualToAnchor(viewAbove.bottomAnchor, constant: 8.0)
        constraint.active = true
    }
    /////
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 350, height: 38 * sharesViews.count - 8)
    }
    
    override class func requiresConstraintBasedLayout() -> Bool {
        return true
    }
    
}