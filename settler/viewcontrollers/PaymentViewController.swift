import UIKit

class PaymentViewController: UIViewController {

    // REPORT Default value on Optional field
    // REPORT Placeholder autoconstraint

    // MARK: Properties
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIBarButtonItem!

    @IBOutlet weak var changePayerButton: UIButton!
    @IBOutlet weak var paymentTotalTextField: UITextField!
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var changeSharesTypeButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var forWhomLabel: UILabel!
    @IBOutlet weak var forWhatLabel: UILabel!

    // Shares view
    private var coefficientSharesView: CoefficientSharesView?
    private var fixedAmountSharesView: FixedAmountSharesView?
    private var sharesView: UIView? {
        get { return usesCoefficients ? coefficientSharesView : fixedAmountSharesView }
    }
    private var sharesViewConstraints = [NSLayoutConstraint]()


    // Payment properties
    var group: Group!
    var payment: Payment?

    var payer: Member?
    var usesCoefficients = true

    var amount: Money {
        get {
            let total = Money(value: paymentTotalTextField.text ?? "")
            return total ?? Money()
        }
    }

    var currency: String {
        get { return (currencyTextField.text ?? "").trim() }
    }

    var shares: [UnmanagedMemberShare] {
        get {
            if usesCoefficients {
                return coefficientSharesView!.shares.map({ share in UnmanagedMemberShare(member: share.member, value: Money(value: "\(share.coefficient)")) })
            }

            return fixedAmountSharesView!.shares
        }
    }

    var name: String {
        get { return (nameTextField.text ?? "").trim() }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up views if editing existing payment
        if let payment = self.payment {
            payer = payment.contributions[0].member

            paymentTotalTextField.text = payment.amount.asString()
            currencyTextField.text = payment.currency

            usesCoefficients = payment.useSharesAsCoefficients

            nameTextField.text = payment.name
            navigationItem.title = payment.name
        }
        else {
            // Hide delete button, when creating new group
            deleteButton.enabled = false
            deleteButton.tintColor = UIColor.clearColor()
            
            payer = group.defaultPayer
            currencyTextField.text = group.defaultCurrency
        }
        
        self.changePayerButton.setTitle(payer!.name, forState: UIControlState.Normal)

        updateSharesViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveChanges(sender: UIBarButtonItem) {
        let errors = getValidationErrors()
        if errors.count > 0 {
            showErrors(errors)
            return
        }
        
        let data = DataService.sharedInstance

        let contributions = [data.createContribution(payer!, value: amount)]
        let shares = self.shares.map({ share in data.createAmountShare(share.member, value: share.hasValue ? share.value : nil) })
        
        if let payment = payment {
            // Editing existing payment
            payment.name = name
            payment.currency = currency
            
            data.deleteObjects(payment.contributions)
            data.deleteObjects(payment.shares)
            
            payment.useSharesAsCoefficients = usesCoefficients
            payment.contributions = contributions
            payment.shares = shares
        }
        else {
            // New payment
            payment = data.createPayment(group, name: name, currency: currency, contributions: contributions, shares: shares, useSharesAsCoefficients: usesCoefficients)
            
            // Add as the first
            group.payments.insert(payment!, atIndex: 0)
            // REPORT Why as first?
        }
        
        try! data.saveChanges()
    
        self.performSegueWithIdentifier("SavePayment", sender: sender)
    }
    
    private func getValidationErrors() -> [String] {
        var errors = [String]()
        
        if amount <= Money() {
            errors += ["How much did you pay?"]
        }
        
        if currency == "" {
            errors += ["What currency?"]
        }
        
        if name == ""{
            errors += ["What did you pay for?"]
        }
        
        return errors
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if sender === deleteButton {
            deletePayment()
        }
    }

    private func deletePayment()  {
        DataService.sharedInstance.deleteObject(payment!)
        try! DataService.sharedInstance.saveChanges()
    }


    @IBAction func selectPayer(sender: UIButton) {
        self.view.endEditing(true)

        let alertController = UIAlertController(title: "Who paid?", message: nil, preferredStyle: .Alert)

        for member in group.members {
            let action = UIAlertAction(title: member.name, style: .Default) { (_) in
                self.payer = member
                self.changePayerButton.setTitle(member.name, forState: UIControlState.Normal)
            }
            alertController.addAction(action)
        }

        let action = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(action)

        self.presentViewController(alertController, animated: true, completion: nil)
    }

    @IBAction func totalAmountUpdated(sender: UIView) {
        if !usesCoefficients {
            fixedAmountSharesView!.paymentTotal = amount
        }
    }

    @IBAction func toggleFixedAmountShares(sender: UIView) {
        usesCoefficients = !usesCoefficients
        updateSharesViews()
    }

    @IBAction func nameChanged(textField: UITextField) {
        navigationItem.title = name != "" ? name : "New payment"
    }

    @IBAction func confirmDelete(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Are you sure?", message: "Do you really want to delete this payment?", preferredStyle: .Alert)

        let cancelAction = UIAlertAction(title: "Nope", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)

        let destroyAction = UIAlertAction(title: "Delete", style: .Destructive) { (action) in
            self.performSegueWithIdentifier("DeletePayment", sender: sender)
        }
        alertController.addAction(destroyAction)

        self.presentViewController(alertController, animated: true, completion: nil)
    }

    func showErrors(errors: [String]) {
        let alertController = UIAlertController(title: "Missing", message: errors.reduce("", combine: { "\($0!)\n\($1)" }), preferredStyle: .Alert)

        let cancelAction = UIAlertAction(title: "Back", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)

        self.presentViewController(alertController, animated: true, completion: nil)
    }

    @IBAction func cancel(sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMode = presentingViewController is UINavigationController

        if isPresentingInAddMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }

    func updateSharesViews() {
        if let currentSharesView = self.sharesView {
            currentSharesView.removeConstraints(sharesViewConstraints)
            currentSharesView.removeFromSuperview()
        }

        if usesCoefficients {
            if coefficientSharesView == nil {
                let shares = payment != nil && payment!.useSharesAsCoefficients
                    ? getConvertedCoefficientShares(payment!.shares)
                    : group.members.map({ (member: $0, coefficient: Int(1)) })

                let sharesViews = CoefficientSharesView()
                sharesViews.initialize(shares)
                self.view.addSubview(sharesViews)
                self.coefficientSharesView = sharesViews
            }
        }
        else if fixedAmountSharesView == nil {
            let shares = payment != nil && !payment!.useSharesAsCoefficients
                ? payment!.shares.map({ share in UnmanagedMemberShare(member: share.member, value: share.hasValue ? share.value : nil) })
                : group.members.map({ member in UnmanagedMemberShare(member: member, value: nil) })

            let sharesViews = FixedAmountSharesView()
            sharesViews.initialize(shares)
            sharesViews.paymentTotal = amount
            self.fixedAmountSharesView = sharesViews
        }

        let sharesView = self.sharesView!
        self.view.addSubview(sharesView)
        layoutSharesView(sharesView)

        let buttonTitle = usesCoefficients ? "Fix amounts" : "Use coefficients"
        changeSharesTypeButton.setTitle(buttonTitle, forState: .Normal)
    }

    private func layoutSharesView(sharesView: UIView) {
        sharesViewConstraints += Constraints.pinToSideMargins(sharesView, margins: self.view.layoutMarginsGuide)
        sharesViewConstraints += Constraints.pinTopToBottom(sharesView, viewAbove: forWhomLabel)
        sharesViewConstraints += Constraints.pinBottomToTop(sharesView, viewBelow: forWhatLabel)
    }

    func getConvertedCoefficientShares(shares: [MemberShare]) -> [(member: Member, coefficient: Int)] {
        return shares.map({ share in (member: share.member, coefficient: share.value.asInt())})
    }
}

