import Foundation
import UIKit

class CoefficientSharesView : UIView {
    
    var sharesViews = [CoefficientShareView]()
    
    var shares: [(member: Member, coefficient: Int)] {
        get {
            var shares = [(member: Member, coefficient: Int)]()
            for shareView in sharesViews {
                shares += [(member: shareView.member!, coefficient: shareView.coefficient!)]
            }
            return shares
        }
    }
    
    func initialize(shares: [(member: Member, coefficient: Int)]) {
        createUiElements(shares)
        layoutUiElements()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func createUiElements(shares: [(member: Member, coefficient: Int)]) {
        self.backgroundColor = UIColor.whiteColor()
        
        for share in shares {
            let shareView = CoefficientShareView()
            shareView.member = share.member
            shareView.coefficient = share.coefficient
            
            sharesViews += [shareView]
            self.addSubview(shareView)
        }
    }
    
    private func layoutUiElements() {
        guard sharesViews.count >= 2 else {
            print("Fail: SharesView must have at least 2 shares.")
            return
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        pinToSideMargins(sharesViews[0])
        // Pin to top of the control
        sharesViews[0].topAnchor.constraintEqualToAnchor(self.topAnchor).active = true
        
        for var i = 1; i < sharesViews.count - 1; i++ {
            let shareView = sharesViews[i]
            pinToSideMargins(shareView)
            pinTopToBottomOf(sharesViews[i - 1], target: shareView)
        }
        
        pinToSideMargins(sharesViews[sharesViews.count - 1])
        // Pin to bottom of the control
        sharesViews[sharesViews.count - 1].bottomAnchor.constraintEqualToAnchor(self.bottomAnchor).active = true
    }
    
    private func pinToSideMargins(pinnedView: UIView) {
        let margins = self
        let leftMarginConstraint = pinnedView.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor)
        leftMarginConstraint.active = true
        let rightMarginConstraint = pinnedView.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor)
        rightMarginConstraint.active = true
    }
    
    private func pinTopToBottomOf(viewAbove: UIView, target: UIView) {
        let constraint = target.topAnchor.constraintEqualToAnchor(viewAbove.bottomAnchor, constant: 8.0)
        constraint.active = true
    }
    /////
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 350, height: 38 * sharesViews.count - 8)
    }
    
    override class func requiresConstraintBasedLayout() -> Bool {
        return true
    }

}