import UIKit

class DebtTableViewCell: UITableViewCell {

    @IBOutlet weak var debtorLabel: UILabel!
    @IBOutlet weak var payerLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
