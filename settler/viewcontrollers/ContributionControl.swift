import UIKit

class ContributionControl: UIView, UIPickerViewDelegate, UIPickerViewDataSource {

    weak var contributorPicker: UIPickerView!
    var pickerData = ["Monia", "Krzysiek", "Usun konto"]
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
//        let contributorPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        let contributorPicker = UIPickerView()
        contributorPicker.delegate = self
        contributorPicker.dataSource = self
        self.contributorPicker = contributorPicker

        let textField = UITextField(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        textField.inputView = contributorPicker
        addSubview(textField)
        
        //addSubview(contributorPicker)
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 200, height: 44)
    }
    

    
    // MARK: UIPickerViewDelegate
    
    // Catpure the picker view selection
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("Selected \(pickerData[row])")
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
    }
    
    // MARK: UIPickerViewDataSource
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
