import UIKit

class PaymentTableViewController: UITableViewController {

    // REPORT: Why that data storage. +Using dataService, what makes it easier to change data access later on.
    // Not Property Lists because it supports only built in data formats, while objects here are complex
    // Core Data seems to be a good choice, because it also can easily integrate with iCloud, allowing to sync data across user devices
    // # Characteristics of data
    // Confidential
    // Changes couple of times a day
    // Fast access
    // Moderate size
    // Caching objects as JSON could be a good solution, if data would be syncrhonized through own web server (e.g. supporting groups sharing)
    // NSCoding is good to ocassionally store objects of different classes. If you have many objects of the same type (e.g. Payments) it is better to use database approach (SQLite or CoreData - SQLite wrapper)
    // NSCoding for storing application state, while CoreData for storing groups/users/payments?
    // NSCoding (NSKeyedArchiver) does not automatically support migrations - it has to be implemented manually
    // http://nshipster.com/nscoding/
    // Isn't PLists (Property Lists) and NSCoding the same?

    weak var group: Group!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = group.name
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "EditPayment" {
            let paymentDetailViewController = segue.destinationViewController as! PaymentViewController

            paymentDetailViewController.group = group

            // Get the cell that generated this segue
            if let selectedPaymentCell = sender as? PaymentTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedPaymentCell)!
                let selectedPayment = group.payments[indexPath.row]
                paymentDetailViewController.payment = selectedPayment
            }
        }
        else if segue.identifier == "AddPayment" {
            let navCtrl = segue.destinationViewController as! UINavigationController
            let paymentDetailViewController = navCtrl.visibleViewController as! PaymentViewController

            paymentDetailViewController.group = group
        }
        else if segue.identifier == "ShowDebts" {
            let navCtrl = segue.destinationViewController as! UINavigationController
            let debtsViewController = navCtrl.visibleViewController as! DebtTableViewController
            
            debtsViewController.debts = group.debts
        }
    }


    @IBAction func savedPayment(sender: UIStoryboardSegue) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            // Updated an existing payment
            tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
        }
        else {
            // Added a new payment - indicate that it was inserted as first item
            let newIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Top)
        }
    }


    @IBAction func deletedPayment(sender: UIStoryboardSegue) {
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else {
            print("Nothing to delete - no item is selected.")
            return
        }

        tableView.deleteRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .Fade)
    }


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group.payments.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "PaymentTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PaymentTableViewCell

        let payment = group.payments[indexPath.row]

        cell.nameLabel.text = payment.name
        cell.amountLabel.text = payment.amount.asString()
        cell.currencyLabel.text = payment.currency
        let contributorsNames = payment.contributions.map({$0.member.name})
        cell.contributorsLabel.text = "by \(contributorsNames.joinWithSeparator(", "))"

        return cell
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            //var payments = group.payments

            let group = self.group
            DataService.sharedInstance.deleteObject(group.payments[indexPath.row])
            //group.payments.removeAtIndex(indexPath.row)

            //group.payments = payments
            // TODO Catch errors
            try! DataService.sharedInstance.saveChanges()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
}
