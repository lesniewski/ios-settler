import UIKit

class GroupTableViewController: UITableViewController {

    var groups = [Group]()
    var editedGroupCell: GroupTableViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addLongPressGestureRecognizerToTable()
        
        loadData()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private func loadData() {
        let loadedGroups = DataService.sharedInstance.getGroups()
        if loadedGroups != nil && loadedGroups!.count > 0 {
            groups = loadedGroups!
            return
        }
        
        // TODO Remove sample data creation before release to AppStore
        createAndSaveSampleData()
        let loadedSampleGroups = DataService.sharedInstance.getGroups()
        if (loadedSampleGroups != nil) {
            groups = loadedSampleGroups!
        }
    }


    private func createAndSaveSampleData() {
        let data = DataService.sharedInstance
        
        let group = data.createGroup("Test Group")
        
        let membersNames = ["Monia", "Krzysiek"]
        let members = membersNames.map({ data.createMember(group, name: $0) })
        group.members = members
        
        let paymentsValues = [
            ("Burger Anarchy", "310", "DKK", members[1], "1", "1"),
            ("Shoes", "499.00", "DKK", members[1], "0", "1"),
            ("Train tickets", "21.59", "CHF", members[0], "1", "2")
        ]
        
        var payments = [Payment]()
        for (name, amount, currency, contributor, coefficient1, coefficient2) in paymentsValues {
            let contribution = data.createContribution(contributor, value: Money(value: amount)!)
            
            let share1 = data.createCoefficientShare(members[0], value: Money(value: coefficient1)!)
            let share2 = data.createCoefficientShare(members[1], value: Money(value: coefficient2)!)
            
            let payment = data.createPayment(group, name: name, currency: currency, contributions: [contribution], shares: [share1, share2], useSharesAsCoefficients: true)
            
            payments += [payment]
        }
        group.payments = payments
        
        // TODO Catch errors
        try! data.saveChanges()
    }

    
    private func addLongPressGestureRecognizerToTable() {
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleCellLongPress:")
        gestureRecognizer.minimumPressDuration = 0.9; //seconds
        self.tableView.addGestureRecognizer(gestureRecognizer)
    }

    
    func handleCellLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        guard gestureRecognizer.state == .Began else {
            return
        }

        let touchPoint = gestureRecognizer.locationInView(self.tableView)
        let touchedCellPath = self.tableView.indexPathForRowAtPoint(touchPoint)
        
        guard touchedCellPath != nil else {
            print("Long press on table view but not on a row.")
            return
        }

        let touchedCell = self.tableView.cellForRowAtIndexPath(touchedCellPath!)
        performSegueWithIdentifier("EditGroup", sender: touchedCell)
    }


    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        editedGroupCell = nil
        
        if segue.identifier == "ShowPayments" {
            let paymentsViewController = segue.destinationViewController as! PaymentTableViewController
            paymentsViewController.group = getSelectedGroup(sender)!
        }
        else if segue.identifier == "EditGroup" {
            let editViewController = segue.destinationViewController as! GroupViewController
            editedGroupCell = sender as? GroupTableViewCell
            editViewController.group = getSelectedGroup(sender)!
        }
        else if segue.identifier == "AddGroup" {
            // Just open the modal
        }
    }
    
    private func getSelectedGroup(sender: AnyObject?) -> Group? {
        guard let selectedCell = sender as? GroupTableViewCell else {
            return nil
        }
        
        let indexPath = tableView.indexPathForCell(selectedCell)!
        return groups[indexPath.row]
    }


    @IBAction func savedGroup(sender: UIStoryboardSegue) {
        if let editedGroupCell = editedGroupCell {
            // Updated an existing group
            let editedIndexPath = tableView.indexPathForCell(editedGroupCell)!
            tableView.reloadRowsAtIndexPaths([editedIndexPath], withRowAnimation: .None)
        }
        else {
            // Added a new group - reload collection and indicate that it was inserted as first item
            groups = DataService.sharedInstance.getGroups()!
            let newIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Top)
        }
    }


    @IBAction func deletedGroup(sender: UIStoryboardSegue) {
        guard let editedGroupCell = editedGroupCell else {
            return
        }

        groups = DataService.sharedInstance.getGroups()! // Reload
        let editedIndexPath = tableView.indexPathForCell(editedGroupCell)!
        tableView.deleteRowsAtIndexPaths([editedIndexPath], withRowAnimation: .Fade)
    }


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "GroupTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! GroupTableViewCell

        let group = groups[indexPath.row]

        cell.nameLabel.text = group.name

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let group = groups[indexPath.row]
            DataService.sharedInstance.deleteObject(group)

            // TODO Catch errors
            try! DataService.sharedInstance.saveChanges()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }

}
