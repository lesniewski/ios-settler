
enum RuntimeError: ErrorType {
    case InvalidOperation
}

enum CreationError: ErrorType {
    case Validation(String)
}