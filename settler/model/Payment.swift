import Foundation
import CoreData


class Payment: NSManagedObject {
    
    @NSManaged var group: Group
    
    @NSManaged private var cdContributions: NSOrderedSet
    @NSManaged private var cdShares: NSOrderedSet
    
    @NSManaged var name: String
    @NSManaged var currency: String
    @NSManaged var created: NSDate
    
    @NSManaged private var cdUseSharesAsCoefficients: NSNumber
    var useSharesAsCoefficients: Bool {
        get { return Bool(cdUseSharesAsCoefficients) }
        set { self.cdUseSharesAsCoefficients = NSNumber(bool: newValue) }
    }
    
    var contributions: [MemberShare] {
        get { return cdContributions.array as! [MemberShare] }
        set { cdContributions = NSOrderedSet(array: newValue) }
    }
    
    var shares: [MemberShare] {
        get { return cdShares.array as! [MemberShare] }
        set { cdShares = NSOrderedSet(array: newValue) }
    }
    
    var amount: Money {
        get {
            return self.contributions.map({ $0.value }).reduce(Money(), combine: { $0 + $1 })
        }
    }
    
    var contributors: [Member] {
        get { return self.contributions.map({ $0.member }) }
    }
    
    var sharesAmounts: [Member:(amount: Money, provided: Bool)] {
        get {
            return useSharesAsCoefficients
                ? getSharesAmountsFromCoefficients()
                : getSharesAmountsFromFixedAmounts()
        }
    }
    
    var balance: [Member:Money] {
        get { return getBalance() }
    }


    private func getSharesAmountsFromCoefficients() -> [Member:(amount: Money, provided: Bool)] {
        let coefficientsSum = shares.map({ $0.value }).reduce(Money(), combine: { $0 + $1 })
        let coefficientMultiplier = self.amount / coefficientsSum
        
        var sharesAmounts = [Member:(amount: Money, provided: Bool)]()
        for share in shares {
            let shareAmount = share.value * coefficientMultiplier
            sharesAmounts[share.member] = (amount: shareAmount, provided: false)
        }
        
        return sharesAmounts
    }

    private func getSharesAmountsFromFixedAmounts() -> [Member:(amount: Money, provided: Bool)] {
        let unsplitAmountPerUnsetMember = getUnsplitAmountPerUnsetMember()
        var sharesAmounts = [Member:(amount: Money, provided: Bool)]()
        for share in shares {
            let shareAmount = share.hasValue ? share.value : unsplitAmountPerUnsetMember
            sharesAmounts[share.member] = (amount: shareAmount, provided: share.hasValue)
        }
        
        return sharesAmounts
    }
    
    private func getUnsplitAmountPerUnsetMember() -> Money {
        let splitAmount = shares.filter({ $0.hasValue }).map({ $0.value }).reduce(Money(), combine: { $0 + $1 })
        let unsplitAmount = self.amount - splitAmount
        let unsetMembersCount = shares.filter({ !$0.hasValue }).count
        let unsplitAmountPerUnsetMember = unsplitAmount / unsetMembersCount
        return unsplitAmountPerUnsetMember
    }

    private func getBalance() -> [Member: Money] {
        var balance = [Member: Money]()
        
        for contribution in contributions {
            balance[contribution.member] = contribution.value
        }
        
        for (member, shareAmount) in sharesAmounts {
            if (balance[member] == nil) {
                balance[member] = Money()
            }
            
            balance[member] = balance[member]! - shareAmount.amount
        }
        
        return balance
    }
}