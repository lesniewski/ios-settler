import Foundation
import CoreData


class MemberShare: NSManagedObject {
    
    @NSManaged var member: Member
    @NSManaged private var share: NSDecimalNumber?

    var value: Money {
        get { return Money(value: share!)! }
        set { share = NSDecimalNumber(string: newValue.asString()) }
    }
    
    var hasValue: Bool {
        get { return share != nil }
    }
    
    func clearValue() {
        share = nil
    }
    
}
