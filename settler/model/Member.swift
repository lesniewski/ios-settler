import Foundation
import CoreData


class Member: NSManagedObject {
    
    @NSManaged var group: Group
    @NSManaged var name: String
    
}
