import Foundation

class UnmanagedMemberShare {
    var member: Member
    private var share: Money?
    
    var value: Money {
        get { return share!.copy() }
        set { share = newValue.copy() }
    }
    
    var hasValue: Bool {
        get { return share != nil }
    }
    
    func clearValue() {
        share = nil
    }
    
    init(member: Member, value: Money?) {
        self.member = member
        self.share = value
    }
}