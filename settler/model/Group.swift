import Foundation
import CoreData


class Group: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var created: NSDate
    
    @NSManaged private var cdMembers: NSOrderedSet
    @NSManaged private var cdPayments: NSOrderedSet

    var members: [Member] {
        get { return cdMembers.array as! [Member] }
        set { cdMembers = NSOrderedSet(array: newValue) }
    }
    
    var payments: [Payment] {
        get { return cdPayments.array as! [Payment] }
        set { cdPayments = NSOrderedSet(array: newValue) }
    }
    
    var balance: [String:[Member: Money]] {
        get {
            let currencies = payments.map({ $0.currency }).unique
            var balanceByCurrencies = [String:[Member: Money]]()
            
            for currency in currencies {
                var balance = [Member: Money]()
                for member in members {
                    balance[member] = Money()
                }
                
                for payment in payments.filter({ $0.currency == currency }) {
                    for (member, paymentBalance) in payment.balance {
                        balance[member] = balance[member]! + paymentBalance
                    }
                }
                
                balanceByCurrencies[currency] = balance
            }
            
            return balanceByCurrencies
        }
    }
    
    var debts: [Debt] {
        get {
            let balanceByCurrencies = balance
            var debts = [Debt]()
            
            for currencyBalance in balanceByCurrencies {
                debts += getDebtsForSingleCurrency(currencyBalance.1, currency: currencyBalance.0)
            }
            
            return debts
        }
    }
    
    var defaultPayer: Member {
        if payments.count > 0 {
            return payments[0].contributions[0].member
        }
        return members[0]
    }
    
    var defaultCurrency: String {
        if payments.count > 0 {
            return payments[0].currency
        }
        return ""
    }
    
    
    func getDebtsForSingleCurrency(balance: [Member: Money], currency: String) -> [Debt]  {
        let sortedBalance = balance.map({ (member: $0.0, balance: $0.1) }).sort({ $0.balance > $1.balance })
        let zero = Money()
        let payers = sortedBalance.filter({ $0.balance > zero }).map({ $0.member })
        let debtors = sortedBalance.filter({ $0.balance < zero }).map({ $0.member })
        
        var membersBalance = balance
        var debts = [Debt]()
        
        var p = 0
        var d = 0
        while p < payers.count && d < debtors.count {
            let payer = payers[p]
            let debtor = debtors[d]
            
            let paidAmount = membersBalance[payer]!
            let debtAmount = membersBalance[debtor]!.absolute()
            
            if paidAmount == debtAmount {
                let debt = Debt(who: debtor, toWhom: payer, amount: paidAmount, currency: currency)
                debts += [debt]
                membersBalance[payer] = zero
                membersBalance[debtor] = zero
                p++
                d++
                continue
            }
            else if paidAmount > debtAmount {
                let debt = Debt(who: debtor, toWhom: payer, amount: debtAmount, currency: currency)
                debts += [debt]
                membersBalance[payer] = paidAmount - debtAmount
                membersBalance[debtor] = zero
                d++
                continue
            }
            else {
                let debt = Debt(who: debtor, toWhom: payer, amount: paidAmount, currency: currency)
                debts += [debt]
                membersBalance[payer] = zero
                membersBalance[debtor] = debtAmount.negative()
                p++
                continue
            }
        }
        
        return debts
    }
}
