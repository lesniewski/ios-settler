import Foundation
import UIKit

class Constraints {
    
    class func pinToSideMargins(pinnedView: UIView, margins: UILayoutGuide) -> [NSLayoutConstraint] {
        let leftMarginConstraint = pinnedView.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor)
        leftMarginConstraint.active = true
        
        let rightMarginConstraint = pinnedView.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor)
        rightMarginConstraint.active = true
        
        return [leftMarginConstraint, rightMarginConstraint]
    }
    
    class func pinToSides(pinnedView: UIView, parent: UIView)  -> [NSLayoutConstraint] {
        let leftMarginConstraint = pinnedView.leadingAnchor.constraintEqualToAnchor(parent.leadingAnchor)
        leftMarginConstraint.active = true
        
        let rightMarginConstraint = pinnedView.trailingAnchor.constraintEqualToAnchor(parent.trailingAnchor)
        rightMarginConstraint.active = true
        
        return [leftMarginConstraint, rightMarginConstraint]
    }
    
    class func pinTopToBottom(pinnedView: UIView, viewAbove: UIView) -> [NSLayoutConstraint] {
        let constraint = pinnedView.topAnchor.constraintEqualToAnchor(viewAbove.bottomAnchor, constant: 8.0)
        constraint.active = true
        
        return [constraint]
    }
    
    class func pinBottomToTop(pinnedView: UIView, viewBelow: UIView) -> [NSLayoutConstraint] {
        let constraint = pinnedView.bottomAnchor.constraintEqualToAnchor(viewBelow.topAnchor, constant: -8.0)
        constraint.active = true
        
        return [constraint]
    }
    
}