import Foundation


class Money {
    
    // REPORT: One of the reasons to wrap NSDecimalNumber: http://natashatherobot.com/swift-nsdecimalnumber/
    
    private static let epsilon:Double = 0.01
    
    private var value:Double
    
    init?(value:String) {
        if let passedValue = Double(value) {
            self.value = passedValue
        }
        else {
            // Must initialize properties before failing initialized. It is a bug: http://stackoverflow.com/a/26497229
            self.value = 0
            return nil
        }
    }
    
    convenience init?(value: NSDecimalNumber) {
        var valueAsString = "not a number"
        if (value != NSDecimalNumber.notANumber()) {
            valueAsString = String(format: "%.2f", value.doubleValue)
        }
        
        self.init(value: valueAsString)
    }
    
    convenience init() {
        self.init(value: 0 as Double)
    }
    
    private init(value: Double) {
        self.value = value
    }
    
    func asString() -> String {
        return String(format: "%.2f", self.value)
    }
    
    func asDouble() -> Double {
        return value
    }
    
    func asInt() -> Int {
        return Int(value)
    }
    
    func copy() -> Money {
        return Money(value: self.value)
    }
    
    func absolute() -> Money {
        return Money(value: abs(self.value))
    }
    
    func negative() -> Money {
        return Money(value: self.value * -1)
    }
}


func +(left: Money, right: Money) -> Money {
    return Money(value: left.value + right.value)
}

func -(left: Money, right: Money) -> Money {
    return Money(value: left.value - right.value)
}

func *(left: Money, right: Money) -> Money {
    return Money(value: left.value * right.value)
}

func /(left: Money, right: Money) -> Money {
    return Money(value: left.value / right.value)
}

func /(left: Money, right: Int) -> Money {
    return Money(value: left.value / Double(right))
}

func ==(left: Money, right: Money) -> Bool {
    return abs(left.value - right.value) <= Money.epsilon
}

func !=(left: Money, right: Money) -> Bool {
    return !(left == right)
}

func >(left: Money, right: Money) -> Bool {
    return left != right && left.value > right.value
}

func <(left: Money, right: Money) -> Bool {
    return left != right && left.value < right.value
}

func >=(left: Money, right: Money) -> Bool {
    return left == right || left > right
}

func <=(left: Money, right: Money) -> Bool {
    return left == right || left < right
}

