import Foundation

class Nillable<T> {
    
    private var _value:T?
    
    private init(value:T?) {
        self._value = value
    }
    
    init(value:T) {
        self.value = value
    }
    
    convenience init() {
        self.init(value: nil)
    }
    
    var value:T {
        get {
            return _value!
        }
        set {
            _value = newValue
        }
    }
    
    var hasValue:Bool {
        get {
            return _value != nil
        }
    }
    
    func clearValue() {
        _value = nil
    }
}