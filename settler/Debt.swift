//
// Created by Krzysztof Andrezej Lesniewski on 30/11/15.
// Copyright (c) 2015 Krzysztof Lesniewski. All rights reserved.
//

import Foundation

class Debt {

    var who: Member
    var toWhom: Member
    var amount: Money
    var currency: String

    init(who: Member, toWhom: Member, amount: Money, currency: String) {
        self.who = who
        self.toWhom = toWhom
        self.amount = amount
        self.currency = currency
    }
}
