import Foundation
import UIKit
import CoreData

class DataService {
    
    static let sharedInstance = DataService()
    
    private let managedObjectContext: NSManagedObjectContext
    
    private init() {
        // Retreive the managedObjectContext from AppDelegate
        managedObjectContext =
            (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    }
    
    func getGroups() -> [Group]? {
        let fetchRequest = NSFetchRequest(entityName: "Group")
        let sortDescriptor = NSSortDescriptor(key: "created", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            return try managedObjectContext.executeFetchRequest(fetchRequest) as? [Group]
        }
        catch {
            return nil
        }
    }
    
//    func getGroupByURI() -> Group {
//
//    }
    
    func createGroup(name: String) -> Group {
        // if (name == "") {
        //     throw CreationError.Validation("Name can not be empty.")
        // }
        
        // if (members.count < 2) {
        //     throw CreationError.Validation("Group must count at least 2 members.")
        // }
        
        let group = NSEntityDescription.insertNewObjectForEntityForName("Group", inManagedObjectContext: self.managedObjectContext) as! Group
        
        group.name = name
        group.created = NSDate()
        
        return group
    }

    func createMember(group: Group, name: String) -> Member {
        let member = NSEntityDescription.insertNewObjectForEntityForName("Member", inManagedObjectContext: self.managedObjectContext) as! Member
        
        member.group = group
        member.name = name
        
        return member
    }
    
    func createPayment(group: Group, name: String, currency: String, contributions: [MemberShare], shares: [MemberShare], useSharesAsCoefficients: Bool) -> Payment {
        
        let payment = NSEntityDescription.insertNewObjectForEntityForName("Payment", inManagedObjectContext: self.managedObjectContext) as! Payment
        
        payment.group = group
        payment.name = name
        payment.currency = currency
        payment.contributions = contributions
        payment.shares = shares
        payment.useSharesAsCoefficients = useSharesAsCoefficients
        payment.created = NSDate()
        
        return payment
    }
    
    func createContribution(member: Member, value: Money) -> MemberShare {
        let contribution = NSEntityDescription.insertNewObjectForEntityForName("MemberShare", inManagedObjectContext: self.managedObjectContext) as! MemberShare

        contribution.member = member
        contribution.value = value

        return contribution
    }

    func createCoefficientShare(member: Member, value: Money) -> MemberShare {
        let share = NSEntityDescription.insertNewObjectForEntityForName("MemberShare", inManagedObjectContext: self.managedObjectContext) as! MemberShare

        share.member = member
        share.value = value

        return share
    }

    func createAmountShare(member: Member, value: Money?) -> MemberShare {
        let share = NSEntityDescription.insertNewObjectForEntityForName("MemberShare", inManagedObjectContext: self.managedObjectContext) as! MemberShare

        share.member = member
        if value != nil {
            share.value = value!
        }

        return share
    }

    func deleteObject(entity: NSManagedObject) {
        self.managedObjectContext.deleteObject(entity)
    }
    
    func deleteObjects(entities: [NSManagedObject]) {
        for entity in entities {
            self.managedObjectContext.deleteObject(entity)
        }
    }

    func saveChanges() throws {
        try self.managedObjectContext.save()
    }
    
}
