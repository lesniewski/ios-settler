import Foundation
import UIKit

class FixedShareView : UIView {
    
    var memberNameLabel: UILabel!
    var fixedAmountSymbolLabel: UILabel!
    var amountTextField: UITextField!

    var targets = [FixedShareViewEvent: [FixedShareEventTarget]]()

    var member: Member! {
        didSet {
            memberNameLabel.text = member.name
        }
    }
    
    var amount: Money! {
        didSet {
            amountTextField.text = amount.asString()
        }
    }

    var amountFixed: Bool = false {
        didSet {
            fixedAmountSymbolLabel.hidden = !amountFixed;
        }
    }

    var enabled: Bool = true {
        didSet {
            amountTextField.enabled = enabled
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        createUiElements()
        layoutUiElements()
    }

    private func createUiElements() {
        memberNameLabel = UILabel()
        memberNameLabel.font = UIFont.systemFontOfSize(20)
        addSubview(memberNameLabel)

        fixedAmountSymbolLabel = UILabel()
        fixedAmountSymbolLabel.text = "🔒"
        fixedAmountSymbolLabel.textAlignment = .Right
        fixedAmountSymbolLabel.font = UIFont.systemFontOfSize(20)
        fixedAmountSymbolLabel.userInteractionEnabled = true
        fixedAmountSymbolLabel.hidden = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "fixedAmountSymbolTapped:")
        fixedAmountSymbolLabel.addGestureRecognizer(tapGestureRecognizer)
        addSubview(fixedAmountSymbolLabel)

        amountTextField = UITextField()
        amountTextField.textAlignment = .Right
        amountTextField.keyboardType = .DecimalPad
        amountTextField.borderStyle = .RoundedRect
        amountTextField.returnKeyType = .Done
        amountTextField.enablesReturnKeyAutomatically = true
        amountTextField.addTarget(self, action: "textFieldEditingDidEnd:", forControlEvents: .EditingDidEnd)
        addSubview(amountTextField)
    }

    private func layoutUiElements() {
        translatesAutoresizingMaskIntoConstraints = false

        memberNameLabel.translatesAutoresizingMaskIntoConstraints = false
        fixedAmountSymbolLabel.translatesAutoresizingMaskIntoConstraints = false
        amountTextField.translatesAutoresizingMaskIntoConstraints = false

        memberNameLabel.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true
        fixedAmountSymbolLabel.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true
        amountTextField.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true

        amountTextField.widthAnchor.constraintEqualToConstant(90.0).active = true
        amountTextField.trailingAnchor.constraintEqualToAnchor(self.trailingAnchor).active = true
        amountTextField.leadingAnchor.constraintEqualToAnchor(fixedAmountSymbolLabel.trailingAnchor, constant: 3.0).active = true
        memberNameLabel.leadingAnchor.constraintEqualToAnchor(self.leadingAnchor).active = true
    }

    func addTarget(target: AnyObject!, action: String, forControlEvent: FixedShareViewEvent) {
        if targets[forControlEvent] == nil {
            targets[forControlEvent] = [FixedShareEventTarget]()
        }
        targets[forControlEvent]! += [FixedShareEventTarget(target: target, action: action)]
    }

    private func fireEvent(event: FixedShareViewEvent) {
        guard let targets = self.targets[event] else {
            return
        }

        for eventTarget in targets {
            eventTarget.target.performSelector(Selector(eventTarget.action), withObject: self)
        }
    }

    func textFieldEditingDidEnd(sender: UITextField) {
        let enteredValue = amountTextField.text ?? ""
        let parsedValue = Money(value: enteredValue)
        if let value = parsedValue {
            if amount != value {
                amount = value
                fireEvent(.AmountChanged)
            }
        }
        else {
            amountTextField.text = amount.asString()
        }
    }

    func fixedAmountSymbolTapped(sender: UILabel) {
        guard amountFixed else {
            return
        }

        amountFixed = false
        fireEvent(.FixDeleted)
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 330, height: 30)
    }
    
    override class func requiresConstraintBasedLayout() -> Bool {
        return true
    }
}

enum FixedShareViewEvent: Int {
    case AmountChanged
    case FixDeleted
}

class FixedShareEventTarget {
    var target: AnyObject
    var action: String
    
    init(target: AnyObject, action: String) {
        self.target = target
        self.action = action
    }
}
