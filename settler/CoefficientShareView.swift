import Foundation
import UIKit

class CoefficientShareView : UIView {

    var memberNameLabel: UILabel!
    var coefficientTextField: UITextField!
    var coefficientStepper: UIStepper!
    
    var member: Member! {
        didSet {
            memberNameLabel.text = member.name
        }
    }
    
    var coefficient: Int! {
        didSet {
            coefficientStepper.value = Double(coefficient)
            coefficientTextField.text = "\(coefficient)"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        createUiElements()
        layoutUiElements()
    }

    private func createUiElements() {
        memberNameLabel = UILabel()
        memberNameLabel.font = UIFont.systemFontOfSize(20)
        addSubview(memberNameLabel)

        coefficientTextField = UITextField()
        coefficientTextField.textAlignment = .Center
        coefficientTextField.keyboardType = .NumberPad
        coefficientTextField.borderStyle = .RoundedRect
        coefficientTextField.clearsOnBeginEditing = true
        coefficientTextField.returnKeyType = .Done
        coefficientTextField.enablesReturnKeyAutomatically = true
        coefficientTextField.addTarget(self, action: "textFieldEditingDidEnd:", forControlEvents: .EditingDidEnd)
        addSubview(coefficientTextField)

        coefficientStepper = UIStepper()
        coefficientStepper.stepValue = 1
        coefficientStepper.value = 1
        coefficientStepper.minimumValue = 0
        coefficientStepper.maximumValue = 99
        coefficientStepper.addTarget(self, action: "stepperValueChanged:", forControlEvents: .ValueChanged)
        addSubview(coefficientStepper)
    }

    private func layoutUiElements() {
        translatesAutoresizingMaskIntoConstraints = false

        memberNameLabel.translatesAutoresizingMaskIntoConstraints = false
        coefficientTextField.translatesAutoresizingMaskIntoConstraints = false
        coefficientStepper.translatesAutoresizingMaskIntoConstraints = false

        memberNameLabel.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true
        coefficientTextField.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true
        coefficientStepper.centerYAnchor.constraintEqualToAnchor(self.centerYAnchor).active = true

        coefficientStepper.trailingAnchor.constraintEqualToAnchor(self.trailingAnchor).active = true
        coefficientStepper.leadingAnchor.constraintEqualToAnchor(coefficientTextField.trailingAnchor, constant: 8.0).active = true
        coefficientTextField.widthAnchor.constraintEqualToConstant(38.0).active = true
        memberNameLabel.leadingAnchor.constraintEqualToAnchor(self.leadingAnchor).active = true
    }

    func stepperValueChanged(sender: UIStepper) {
        let newValue = Int(sender.value)
        coefficient = newValue
    }

    func textFieldEditingDidEnd(sender: UITextField) {
        let enteredValue = coefficientTextField.text ?? ""
        let parsedValue = Int(enteredValue)
        if let value = parsedValue {
            coefficient = value
        }
        else {
            coefficientTextField.text = "\(coefficient)"
        }
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 350, height: 30)
    }
    
    override class func requiresConstraintBasedLayout() -> Bool {
        return true
    }
}
